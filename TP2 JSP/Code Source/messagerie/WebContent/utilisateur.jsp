<%@ page errorPage = "/WEB-INF/erreur.jsp" import = "messagerie_metier.*" %>

<%@ include file = "/WEB-INF/jspf/navigation.jspf" %>
 
 <%! boolean nouveau; %> 
 <%  nouveau = request.getParameter("authentification").equals("nouveau"); %>
  
  <h3 align="center">
     <font color="green"><%= (nouveau ? "Demande d'inscription" : "Vos r�f�rences") %></font>
 </h3>

 <form action="<%= (nouveau ? "validerutilisateur.jsp": "controleidentite.jsp") %>" method="post">
     <p><table border="1" cellpadding="3" cellspacing="2" width="90%" align="center">
         <tr>
             <td bgcolor="#FF9900" width="100"><b>Nom</b></td>
             <td><input type="text" name="nom"></td>
         </tr>
         <tr>
             <td bgcolor="#FF9900" width="100"><b>Pr�nom</b></td>
             <td><input type="text" name="prenom"></td>
         </tr>
         <tr>
             <td bgcolor="#FF9900" width="100"><b>Mot de passe</b></td>
             <td><input type="password" name="motDePasse"></td>
         </tr>
     </table></p>
     <p align="center"><input type="submit" value="Valider"></p>
 </form>
 
<%@ include file = "/WEB-INF/jspf/pieds.jspf" %>