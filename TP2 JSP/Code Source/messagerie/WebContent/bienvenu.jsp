<%@ page errorPage = "/WEB-INF/erreur.jsp" import="messagerie_metier.*" %>

<%! int idPersonne = 1; %>
<%! String identite = "A tout le monde"; %>
 
 <% 
     Personne utilisateur = (Personne) session.getAttribute("utilisateur"); 
     String authentification = request.getParameter("authentification");
     if (utilisateur == null && authentification == null) { 
 %>
     <jsp:forward page="/WEB-INF/authentifier.jsp" />
 <%
     } else if (utilisateur == null && authentification != null && !authentification.equals("anonyme")) {
  %>
     <jsp:forward page="utilisateur.jsp">
         <jsp:param name="authentification" value="<%= authentification %>" />
     </jsp:forward> 
 <%
 	} else if (utilisateur != null) {
         idPersonne = utilisateur.identificateur();
         identite = utilisateur.getPrenom()+" "+utilisateur.getNom();
    }
 %>
 
<font face="Arial">
 
<p><table border="1" cellpadding="3" cellspacing="2" width="90%" align="center">    
  <tr bgcolor="#FF6600">
          <th>Sujet</th>
         <th>Message</th>
     </tr>
     <%   
         ListeMessages listeMessages = new ListeMessages(idPersonne);
         int ligne = 0;
         while (listeMessages.suivant()) {
     %>
     <tr bgcolor="<%= ligne++ % 2 == 0 ? "#FFFF66" : "#FFCC00" %>">
         <td><b><%= listeMessages.sujet() %></b></td>
         <td><%= listeMessages.texte() %></td>
     </tr>
     <%
         }
         listeMessages.arret();
     %>
 </table></p>
 </font>
 
 <%@ include file = "/WEB-INF/jspf/pieds.jspf" %>