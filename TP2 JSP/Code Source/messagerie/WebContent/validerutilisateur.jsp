<%@ page errorPage="/WEB-INF/erreur.jsp" import="messagerie_metier.*"%> 
 
<%@ include file="/WEB-INF/jspf/navigation.jspf"%>

<h3 align="center">Confirmation de votre demande d"inscription</h3>

<jsp:useBean id="utilisateur" class="messagerie_metier.Personne" scope="session">
  <jsp:setProperty name="utilisateur" property="*" />

  <p>
    <table
      border="1"
      cellpadding="3"
      cellspacing="2"
      width="90%"
      align="center"
    >
      <tr>
        <td bgcolor="#FF9900" width="100"><b>Nom</b></td>
        <td><jsp:getProperty name="utilisateur" property="nom"/></td>
      </tr>
      <tr>
        <td bgcolor="#FF9900" width="100"><b>Pr�nom</b></td>
        <td><jsp:getProperty name="utilisateur" property="prenom"/></td>
      </tr>
      <tr>
        <td bgcolor="#FF9900" width="100"><b>Mot de passe</b></td>
        <td><jsp:getProperty name="utilisateur" property="motDePasse"/></td>
      </tr>
    </table>
  </p>
</jsp:useBean>

<h3 align="center">
  <% if (!utilisateur.enregistrer()) { %>
  <font color="red">ATTENTION : Utilisateur d�ja enregistr�</font>
  <% session.removeAttribute("utilisateur"); } else { %>
  <font color="green">Nouvel utilisateur enregistr�</font>
  <% } %>
</h3>

<%@ include file="/WEB-INF/jspf/pieds.jspf"%>
