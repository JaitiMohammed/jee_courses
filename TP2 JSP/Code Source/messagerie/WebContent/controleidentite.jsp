<%@ page errorPage = "/WEB-INF/erreur.jsp" import="messagerie_metier.*" %>
<jsp:useBean id="utilisateur" class="messagerie_metier.Personne" scope="session">
<jsp:setProperty name="utilisateur" property="*" />
</jsp:useBean>

<%  if (!utilisateur.authentifier()) {  
        session.removeAttribute("utilisateur");
    }
%>
     
<jsp:forward page="bienvenue.jsp" />