package messagerie_metier;



import java.sql.SQLException;


public class Personne extends ConnexionDB {
	private String nom;
	private String prenom;
	private String motDePasse;

	public Personne() {
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom.toUpperCase();
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom.substring(0, 1).toUpperCase() + prenom.substring(1, prenom.length()).toLowerCase();
	}

	public String getMotDePasse() {
		return this.motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public boolean enregistrer() {
		if (existeDeja())
			return false;
		else {
			miseAJour("INSERT INTO personne (nom, prenom, motDePasse) VALUES (\"" + nom + "\",\"" + prenom + "\",\""
					+ motDePasse + "\")");
			return true;
		}
	}

	private boolean existeDeja() {
		lire("SELECT * FROM personne WHERE nom=\"" + nom + "\" AND prenom=\"" + prenom + "\"");
		return suivant();

	}

	public int identificateur() {
		lire("SELECT idPersonne FROM personne WHERE nom=\"" + nom + "\" AND prenom=\"" + prenom + "\"");
		suivant();
		try {
			return resultat.getInt("idPersonne");
		} catch (SQLException ex) {
			return 1;
		}
	}
	public boolean authentifier() {
	lire("SELECT * FROM personne WHERE nom=\""+nom+"\" AND prenom=\""+prenom+"\" AND motDePasse=\""+motDePasse+"\"");
		return suivant();      
	 }
}