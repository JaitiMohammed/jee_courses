package messagerie_metier;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnexionDB{
   private Connection connexion;
   private Statement instruction;
   protected ResultSet resultat;

   public ConnexionDB() {
      try {
         Class.forName("com.mysql.jdbc.Driver");
         connexion = DriverManager.getConnection("jdbc:mysql://localhost/messagerie", "root", "");
         instruction = connexion.createStatement();
      } catch (ClassNotFoundException ex) {
         System.err.println("Problème de pilote");
      } catch (SQLException ex) {
         System.err.println("Base de données non trouvée ou requète incorrecte");
      }
   }

   public void lire(String requete) {
      try {
         resultat = instruction.executeQuery(requete);
      } catch (SQLException ex) {
         System.err.println("Requete incorrecte " + requete);
      }
   }

   public void miseAJour(String requete) {
      try {
         instruction.executeUpdate(requete);
      } catch (SQLException ex) {
         System.err.println("Requete incorrecte " + requete);
      }
   }

   public boolean suivant() {
      try {
         return resultat.next();
      } catch (SQLException ex) {
         return false;
      }
   }

   public void arret() {
      try {
         connexion.close();
      } catch (SQLException ex) {
         System.err.println(" Erreur sur l'arret de la connexion à la base de données");
      }
   }
}
