<%@page import="java.sql.Date"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Metier.Patient"%>
<%@page import="Metier.Medecin"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

	<%
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/clinique","root","");
		//Medecin m = new Medecin();
		//Patient p = new Patient();
		Medecin m = new Medecin(1,"ALI","Medcine generale");
		Patient p = new Patient(1,"Mohammed",0656565656);
		
		String sql1 = "INSERT INTO `patient`(`id`, `nom`, `tel`) VALUES (?,?,?)";
		String sql2 = "INSERT INTO `medecin`(`id`, `nom`, `speciality`) VALUES(?,?,?)";
		
		
		PreparedStatement pr = conn.prepareStatement(sql1);
		pr.setInt(1,p.getId());
		pr.setString(2,p.getNom());
		pr.setInt(3, p.getTel());
		pr.executeUpdate();
		
		
		PreparedStatement pr2 = conn.prepareStatement(sql2);
		pr2.setInt(1, m.getId());
		pr2.setString(2, m.getNom());
		pr2.setString(3, m.getSpeciality());
		
		
		pr2.executeUpdate();
		
		String sql3 = "INSERT INTO `rdv`(`patient`, `medecin`, `date`) VALUES(?,?,?)";

		PreparedStatement pr3 = conn.prepareStatement(sql3);
		pr3.setString(1,p.getNom());
		pr3.setString(2,m.getNom());
		pr3.setObject(3, "22/02/2020");
		
		pr3.executeUpdate();
		
	%>
<body>
	<h2> Les donn�es sont bient enregistr�es</h2>
</body>
</html>