package Metier;

import java.io.Serializable;

public class Patient implements Serializable{
	private int id;
	private String nom;
	private int tel;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public Patient() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Patient(int id, String nom, int tel) {
		super();
		this.id = id;
		this.nom = nom;
		this.tel = tel;
	}
	
	
}
