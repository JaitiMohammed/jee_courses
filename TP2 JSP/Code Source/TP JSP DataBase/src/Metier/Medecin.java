package Metier;

import java.io.Serializable;

public class Medecin implements Serializable{
	private int id;
	private String nom;
	private String speciality;
	public Medecin() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Medecin(int id, String nom, String speciality) {
		super();
		this.id = id;
		this.nom = nom;
		this.speciality = speciality;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
	
	
	

}
