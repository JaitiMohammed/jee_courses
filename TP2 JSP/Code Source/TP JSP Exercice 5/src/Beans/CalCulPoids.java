package Beans;

public class CalCulPoids {
	private char sexe ;
	private double taille ;
	private double poidId;
	
	
	public CalCulPoids() {
		
	}

	
	public double calculPD() {
		if(this.sexe=='f') {
			this.poidId=72.7*this.taille-58;
			return this.poidId;
		}
		else{
			this.poidId=62.1*this.taille-44.7;
			return this.poidId;
		}
	}
	public char getSexe() {
		return sexe;
	}


	public void setSexe(char sexe) {
		this.sexe = sexe;
	}


	public double getTaille() {
		return taille;
	}


	public void setTaille(double taille) {
		this.taille = taille;
	}


	public double getPoidId() {
		return poidId;
	}


	public void setPoidId(double poidId) {
		this.poidId = poidId;
	}
	
	
}
