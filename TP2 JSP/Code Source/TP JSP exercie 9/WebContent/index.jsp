<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<jsp:useBean id="time1" class="TIME.TimeBean" scope="page" />
<jsp:useBean id="time2" class="TIME.TimeBean" scope="request" />
<jsp:useBean id="time3" class="TIME.TimeBean" scope="session" />
<jsp:useBean id="time4" class="TIME.TimeBean" scope="application" />
<body>

	Time 1 :
	Hours <jsp:getProperty property="hours" name="time1"/>
	Minutes <jsp:getProperty property="minutes" name="time1"/>
	<br/>
	
	
	Time 2 :
	Hours <jsp:getProperty property="hours" name="time2"/>
	Minutes <jsp:getProperty property="minutes" name="time2"/>
	
	<br/>
	Time 3 :
	Hours <jsp:getProperty property="hours" name="time3"/>
	Minutes <jsp:getProperty property="minutes" name="time3"/>
	<br/>
	
	Time 4 :
	Hours <jsp:getProperty property="hours" name="time4"/>
	Minutes <jsp:getProperty property="minutes" name="time4"/>

</body>
</html>