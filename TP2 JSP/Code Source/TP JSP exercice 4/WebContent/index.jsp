<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<jsp:useBean id="Counter1" scope="session" class="compteur.Compteur" />
<jsp:setProperty property="nombre" name="Counter1" value="1" />
<jsp:useBean id="Counter2" scope="request" class="compteur.Compteur" />
<jsp:setProperty property="nombre" name="Counter2" value="2" />
<jsp:useBean id="Counter3" scope="application" class="compteur.Compteur" />
<jsp:setProperty property="nombre" name="Counter3" value="3" />
	<% Counter1.incrementer() ; Counter2.incrementer(); Counter3.incrementer(); %>
<body>
	<h2><%=  Counter1.getNombre()%></h2><br/>
	<h2><%=  Counter2.getNombre()%></h2><br/>
	<h2><%=  Counter3.getNombre()%></h2>
</body>
</html>