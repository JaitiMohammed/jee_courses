package modele;

public class LigneDeCommande {
	private int NumCommande;
	private String CodeArticle ;
	private int QteCde ;
	public LigneDeCommande(int numCommande, String codeArticle, int qteCde) {
		super();
		NumCommande = numCommande;
		CodeArticle = codeArticle;
		QteCde = qteCde;
	}
	public LigneDeCommande() {
		super();
	}
	public int getNumCommande() {
		return NumCommande;
	}
	public void setNumCommande(int numCommande) {
		NumCommande = numCommande;
	}
	public String getCodeArticle() {
		return CodeArticle;
	}
	public void setCodeArticle(String codeArticle) {
		CodeArticle = codeArticle;
	}
	public int getQteCde() {
		return QteCde;
	}
	public void setQteCde(int qteCde) {
		QteCde = qteCde;
	}
	
	
}
