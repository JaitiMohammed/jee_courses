package modele;

public class Categorie {
	private int RefCat ;
	private String cat;
	public Categorie(int refCat, String cat) {
		super();
		RefCat = refCat;
		this.cat = cat;
	}
	public Categorie() {
		super();
	}
	public int getRefCat() {
		return RefCat;
	}
	public void setRefCat(int refCat) {
		RefCat = refCat;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	
	
}
