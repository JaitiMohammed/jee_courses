package modele;

public class Commande {
	private int NumCommande;
	private int CodeClient ;
	private String DateCommande;
	public Commande(int numCommande, int codeClient, String dateCommande) {
		super();
		NumCommande = numCommande;
		CodeClient = codeClient;
		DateCommande = dateCommande;
	}
	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getNumCommande() {
		return NumCommande;
	}
	public void setNumCommande(int numCommande) {
		NumCommande = numCommande;
	}
	public int getCodeClient() {
		return CodeClient;
	}
	public void setCodeClient(int codeClient) {
		CodeClient = codeClient;
	}
	public String getDateCommande() {
		return DateCommande;
	}
	public void setDateCommande(String dateCommande) {
		DateCommande = dateCommande;
	}
	
	
	
}
