package dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnexionDB {

	private static Connection connection ;
	private static String url = "jdbc:mysql://localhost:3306/commandemvc";
	private static String username ="root";
	private static String password = "";
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection
					(url,username,password);
			System.out.println("Database Successfully Connected on 3306");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static Connection getConnection() {
		return connection;
	}
	
}