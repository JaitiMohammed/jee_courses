package dao;

import java.util.List;

import modele.Article;
import modele.Categorie;
import modele.Client;
import modele.Commande;
import modele.LigneDeCommande;

public interface IMetierDAO {
	
	public boolean addClient(Client c);
	public int getIdByName(String nom);
	public void deleteClient(String nom);
	public boolean findByEmailPass(String email,String password);
	public String findNameByEmail(String email);
	public List<Client> getAll();
	
	public void addArticle(Article a , int c);
	public List<Article> getAllArticle();
	public void addCategorie(Categorie c);
	public List<Article> getArticle(String categorie);
	public void AddCommande(Commande c);
	
	public void AddLCommande(LigneDeCommande l );
	
	public List<LigneDeCommande> getAllCommandes();
	List<Commande> getAllCommandes(String nom);
	
	
}
