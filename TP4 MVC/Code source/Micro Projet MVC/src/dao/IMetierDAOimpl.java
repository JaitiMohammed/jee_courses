package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modele.Article;
import modele.Categorie;
import modele.Client;
import modele.Commande;
import modele.LigneDeCommande;

public class IMetierDAOimpl implements IMetierDAO {
	Connection conn = ConnexionDB.getConnection();
	@Override
	public boolean addClient(Client c) {
		PreparedStatement pr ;
		String sql = "INSERT INTO client VALUES (NULL,?,?,?,?,?)";
		
		try {
			pr= conn.prepareStatement(sql);
			pr.setString(1, c.getNom());
			pr.setString(2, c.getPrenom());
			pr.setString(3, c.getAdresse());
			pr.setString(4, c.getEmail());
			pr.setString(5, c.getPassword());
			pr.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true ;
	}

	@Override
	public void deleteClient(String nom) {
		PreparedStatement pr ;
		String sql = "Delete from client where nom =?";
		try {
			pr = conn.prepareStatement(sql);
			pr.setString(1, nom);
			pr.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean findByEmailPass(String email, String password) {
		String query = "SELECT * FROM client WHERE email=? and password = ?";
		PreparedStatement pr ;
		boolean result = false;
		try {
			pr = conn.prepareStatement(query);
			pr.setString(1, email);
			pr.setString(2, password);
			ResultSet rs =  pr.executeQuery();
			
			if(rs.next()) {
				return result=true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public String findNameByEmail(String email) {
		String query = "SELECT nom FROM client WHERE email=?";
		PreparedStatement pr ;
		String nom = null;
		try {
			pr = conn.prepareStatement(query);
			pr.setString(1, email);
			ResultSet rs =  pr.executeQuery();
			
			while(rs.next()) {
				nom = rs.getString("nom");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return nom;
	}

	@Override
	public List<Client> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addCategorie(Categorie c) {
		PreparedStatement ps;
		String sql = "INSERT INTO categorie VALUES (?,?)";
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, c.getRefCat());
			ps.setString(2, c.getCat());
			ps.executeUpdate();
			System.out.println("Bien add");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void addArticle(Article a , int cat) {
		
		String query = "INSERT INTO article VALUES (?,?,?,?,?,?)";
		PreparedStatement pr ;
		try {
			pr = conn.prepareStatement(query);
			pr.setString(1,a.getCodeArticle());
			pr.setString(2,a.getDesignation());
			pr.setDouble(3, a.getPrix());
			pr.setInt(4, a.getStock());
			pr.setInt(5,cat);
			pr.setString(6, a.getPhoto());
			pr.executeUpdate();
			System.out.println("Bien add");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Article> getAllArticle() {
		List<Article> ListeArticle = new ArrayList<Article>();
		
		String query = "Select * from article";
		PreparedStatement pr ;
		
		try {
			pr = conn.prepareStatement(query);
			ResultSet rs = pr.executeQuery();
			
			
			
			while(rs.next()) {
				Article a = new Article();
				a.setCodeArticle(rs.getString("CodeArticle"));
				a.setCategorie(rs.getInt("Categorie"));
				a.setDesignation(rs.getString("Designation"));
				a.setStock(rs.getInt("Stock"));
				a.setPrix(rs.getDouble("Prix"));
				a.setPhoto(rs.getString("Photo"));
				ListeArticle.add(a);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ListeArticle;
	}

	@Override
	public List<Article> getArticle(String categorie) {
		PreparedStatement pr1;
		PreparedStatement pr2;
		List<Article> ListeArticle = new ArrayList<Article>();
		int RefCat = 0 ;
		String query1 = "select RefCat from categorie where Cat  = ?";
		String query2 = "select * from article where Categorie = ?" ;
		try {
			pr1 = conn.prepareStatement(query1);
			pr1.setString(1, categorie);
			ResultSet rs =  pr1.executeQuery();
			while(rs.next()) {
				RefCat = rs.getInt("RefCat");
				pr2 = conn.prepareStatement(query2);
				pr2.setInt(1, RefCat);
				ResultSet rs2 = pr2.executeQuery();
				while(rs2.next()) {
					Article a = new Article();
					
					a.setCodeArticle(rs.getString("CodeArticle"));
					a.setCategorie(rs.getInt("Categorie"));
					a.setDesignation(rs.getString("Designation"));
					a.setStock(rs.getInt("Stock"));
					a.setPrix(rs.getDouble("Prix"));
					a.setPhoto(rs.getString("Photo"));
					ListeArticle.add(a);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ListeArticle;
	}

	@Override
	public void AddCommande(Commande c) {
		
		String query = "INSERT INTO commande VALUES (?,?,?)";
		PreparedStatement pr ;
		
		try {
			pr = conn.prepareStatement(query);
			pr.setInt(1, c.getNumCommande());
			pr.setInt(2, c.getCodeClient());
			pr.setString(3, c.getDateCommande());
			pr.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void AddLCommande(LigneDeCommande l ) {
		
		String query = "INSERT INTO lignecommande VALUES (?,?,?)";
		PreparedStatement pr ;
		
		try {
			pr = conn.prepareStatement(query);
			pr.setInt(1, l.getNumCommande());
			pr.setString(2, l.getCodeArticle());
			pr.setInt(3, l.getQteCde());
			pr.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	
	
}

	@Override
	public int getIdByName(String nom) {
		String query = "select id from client where nom = ?";
		PreparedStatement pr ;
		
		try {
			pr = conn.prepareStatement(query);
			pr.setString(1, nom);
			
			ResultSet rs = pr.executeQuery();
			if(rs.next()) {
				int id = rs.getInt("id");
				return id;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public List<LigneDeCommande> getAllCommandes() {
		List<LigneDeCommande> listCommande = new ArrayList<LigneDeCommande>();
		String query="select * from lignecommande where ";
		
		PreparedStatement pr;
		
		try {
			pr = conn.prepareStatement(query);
			ResultSet rs = pr.executeQuery();
			
			while(rs.next()) {
				LigneDeCommande lc = new LigneDeCommande();
				
				lc.setCodeArticle(rs.getString("CodeArticle"));
				lc.setNumCommande(rs.getInt("NumCommande"));
				lc.setQteCde(rs.getInt("QteCde"));
				listCommande.add(lc);
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return listCommande;
	}

	@Override
	public List<Commande> getAllCommandes(String nom) {
		List<Commande> listCommande = new ArrayList<Commande>();
		String query1 = "select id from client where nom = ?";
		String query="select * from commande where CodeClient = ?";
		
		PreparedStatement pr;
		PreparedStatement pr2;
		try {
			pr = conn.prepareStatement(query);
			pr2 = conn.prepareStatement(query1);
			
			pr2.setString(1, nom);
			ResultSet rs1 = pr2.executeQuery();
			
			while(rs1.next()) {
				int id = rs1.getInt("id");
			
			pr.setInt(1, id);
			ResultSet rs = pr.executeQuery();
			
			while(rs.next()) {
				Commande c = new Commande();
				c.setCodeClient(rs.getInt("CodeClient"));
				c.setDateCommande(rs.getString("DataCommande"));
				c.setNumCommande(rs.getInt("NumCommande"));
				listCommande.add(c);
			}
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		return listCommande;
	}

	
}
