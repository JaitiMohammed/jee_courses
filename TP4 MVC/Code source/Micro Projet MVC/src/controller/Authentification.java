package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IMetierDAO;
import dao.IMetierDAOimpl;
import modele.Client;

/**
 * Servlet implementation class Authentification
 */
@WebServlet(urlPatterns = {"/auth"},loadOnStartup = 1)
public class Authentification extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierDAO metier ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Authentification() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	metier = new IMetierDAOimpl();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.getRequestDispatcher("/WEB-INF/view/components/inscrire.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// récupération des champs forumailre de nouveau client 
		
		String nom = request.getParameter("name");
		String prenom = request.getParameter("prenom");
		String adresse = request.getParameter("adresse");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		// creation d'un nouveau client
		Client c = new Client();
		c.setNom(nom);
		c.setPrenom(prenom);
		c.setAdresse(adresse);
		c.setEmail(email);
		c.setPassword(password);
		
		boolean isOk = metier.addClient(c);
		request.setAttribute("isOk", isOk);
		doGet(request, response);
	}

}
