package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IMetierDAO;
import dao.IMetierDAOimpl;
import modele.Commande;

/**
 * Servlet implementation class CommandeUser
 */
@WebServlet("/CommandeUser")
public class CommandeUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierDAO metier ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommandeUser() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	metier = new IMetierDAOimpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("user");
		
		List<Commande> listCommande = metier.getAllCommandes(user);
		request.setAttribute("Commandes", listCommande);
		request.getRequestDispatcher("WEB-INF/view/components/suivreCommande.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
