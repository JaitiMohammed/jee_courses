package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IMetierDAO;
import dao.IMetierDAOimpl;

/**
 * Servlet implementation class Identification
 */
@WebServlet("/identifier")
public class Identification extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierDAO metier ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Identification() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	metier = new IMetierDAOimpl();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/view/components/identifier.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		
		String nom = metier.findNameByEmail(email);
		
		boolean isOk = metier.findByEmailPass(email, password);
		
		Cookie nomCookie = new Cookie("nomCookie", nom);
		nomCookie.setMaxAge(60*60);
		Cookie PasswordCookie = new Cookie("passwordCookie", password);
		nomCookie.setMaxAge(60*60);
		HttpSession session = request.getSession(true);
		if(isOk == true ) {
			response.addCookie(nomCookie);
			response.addCookie(PasswordCookie);
			
			session.setAttribute("username", nomCookie);
			request.getRequestDispatcher("WEB-INF/view/components/accueil.jsp")
			.forward(request, response);
		}else {
			request.setAttribute("isOk", isOk);
			doGet(request, response);
		}
		
	}

}
