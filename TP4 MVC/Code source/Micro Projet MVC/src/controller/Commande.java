package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IMetierDAO;
import dao.IMetierDAOimpl;
import modele.LigneDeCommande;

/**
 * Servlet implementation class Commande
 */
@WebServlet("/Commander")
public class Commande extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierDAO metier;
	private int counter = 0;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Commande() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	metier = new IMetierDAOimpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String code = request.getParameter("code");
		int stock = Integer.parseInt(request.getParameter("stock"));
		
		String nom = request.getParameter("user");
		request.getRequestDispatcher("/WEB-INF/view/components/commande.jsp").forward(request, response);

		
		if(code !=null && stock != 0 && nom!=null) {
		
		int idc = metier.getIdByName(nom);
		
		modele.Commande c = new modele.Commande(counter++,idc,"07/03/2020");
		LigneDeCommande lc = new LigneDeCommande(c.getNumCommande(),code,stock);

		metier.AddCommande(c);
		
		metier.AddLCommande(lc);
		
		
		
		List<LigneDeCommande> listeLC = metier.getAllCommandes();
		request.setAttribute("listeLC", listeLC);
		request.getRequestDispatcher("/WEB-INF/view/components/commande.jsp").forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
