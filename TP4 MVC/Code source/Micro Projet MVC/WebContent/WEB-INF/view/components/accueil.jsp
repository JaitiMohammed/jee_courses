<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script type="text/javascript" src="bootstrap.js"></script>

</head>
<body>

	<c:set var="nom" value="${username}" scope="session" />
	<div class="container">

		<nav class="navbar navbar-expand navbar-light bg-light">
			<a class="navbar-brand" href="index.jsp">Gestion de commandes </a>
			<a class="nav-item" href="./deconnecter" style="margin-left: 50%;">Déconnecté</a>
		</nav>
		<h1>
			Bienvenue
			<c:out value="${nom.getValue()}"></c:out>
		</h1>
		
		
		<h4><a href="./catalogue">Consulter le catalogue</a></h4>
		<h4><a href="./CommandeUser?user=${nom.getValue()}">Suivre vos commandes</a></h4>
		<h4><a>Visualiser le panier</a></h4>

		
	</div>





</body>
</html>
