<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script type="text/javascript" src="bootstrap.js"></script>

</head>
<body>




	<div class="container">
		<h1>Liste des produits</h1>
		<table border="1" class="table table-striped table-inverse table-responsive">
			<thead class="thead-inverse">
			<tr>
				<th>Code article</th>
				<th>Designation</th>
				<th>Prix</th>
				<th>Stock</th>
				<th>Categorie</th>
				<th>Photo</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			
			<c:forEach var="article" items="${articles}">
			<tr>
					<td><c:out value="${article.getCodeArticle()}" /></td>
					<td><c:out value="${article.getDesignation()}" /></td>
					<td><c:out value="${article.getPrix()}" /></td>
					<td><c:out value="${article.getStock() }" /></td>
					<td><c:out value="${article.getCategorie()}" /></td>
					<td><img src="<c:out value="${article.getPhoto()}"/>"></td>
					<td><a href="./Commander?code=${article.getCodeArticle()}&stock=${article.getStock()}&user=${username.getValue()}">Ajouter au panier</a></td>
				
			</tr>
			</c:forEach>
			
		</tbody>
		</table>
	</div>

</body>
</html>
