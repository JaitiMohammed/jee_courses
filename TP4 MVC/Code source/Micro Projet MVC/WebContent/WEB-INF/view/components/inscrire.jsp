<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="ISO-8859-1" />
    <title>Inscrivez-vous</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <script type="text/javascript" src="bootstrap.js"></script>
  </head>
  <body>
    <div class="container">
      <h1>Inscrivez-vous</h1>
      
      
      
    <c:set var="isOk" value="${isOk}" scope="request"/>
	    <c:if test="${isOk=='true'}">
	    	<div class="alert-success">
	    	<h3><c:out value="Bien Enregist�"/></h3>
	    	</div>
	    </c:if>
      <form action="auth" method="post" class="form-group">
        <label>Nom :</label>
        <input
          type="text"
          class="form-control"
          name="name"
          id="InputName"
          placeholder="Votre Nom"
        />
        <label>Prenom :</label>
        <input
          type="text"
          class="form-control"
          name="prenom"
          id="InputPrenom"
          placeholder="Votre Prenom"
        />
        <label>Adresse</label>
        <input
          type="text"
          class="form-control"
          name="adresse"
          id="InputAdresse"
          placeholder="Votre Adresse"
        />
        <label>Email</label>
        <input
          type="email"
          class="form-control"
          name="email"
          id="InputEmail"
          placeholder="Votre Email"
        />
        <label>Mot de passe</label>
        <input
          type="password"
          class="form-control"
          name="password"
          id="InputPassword"
          placeholder="Votre Password"
        />
         <div class="submit-btn"><button type="submit" class=" btn-small btn-block btn btn-primary">Submit</button></div>
      </form>
    </div>
  </body>
</html>
