<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script type="text/javascript" src="bootstrap.js"></script>
</head>
<body>

		<div class="container">
		
			<h1>Liste des commandes � suivre </h1>
		
			<table border="1" class="table table-striped table-inverse table-responsive">
				<tr>
					<th>numCommande</th>
					<th>codeClient</th>
					<th>dateCommande</th>
				</tr>
			
			<c:forEach items="${Commandes}" var="co">
			<tr>
				<td><c:out value="${co.getNumCommande()}"/></td>
				<td><c:out value="${co.getCodeClient()}"/></td>
				<td><c:out value="${co.getDateCommande()}"/></td>
			</tr>
			</c:forEach>
			</table>
		
		</div>

</body>
</html>