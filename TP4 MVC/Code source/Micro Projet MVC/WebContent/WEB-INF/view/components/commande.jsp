<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vos commandes</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script type="text/javascript" src="bootstrap.js"></script>
</head>
<body>

<div class="container">

	<h1>Vos commandes</h1>
	<table border="1" class="table table-striped table-inverse table-responsive">
		<tr>
			<th>Numero Commande</th>
			<th>Code Article</th>
			<th>Quantite</th>
		</tr>
		
		<c:forEach items="${listeLC}" var="lc">
		<tr>
			<td><c:out value="${lc.getNumCommande()}"></c:out></td>
			<td><c:out value="${lc.getCodeArticle()}"></c:out></td>
			<td><c:out value="${lc.getQteCde()}"></c:out></td>
			</tr>
		</c:forEach>
		
	</table>
</div>
</body>
</html>