<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="ISO-8859-1" />
    <title>Identifier vous</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <script type="text/javascript" src="bootstrap.js"></script>
  </head>
  <body>
  
  
  
  
    <div class="container">
    
    
      
    <c:set var="isOk" value="${isOk}" scope="request"/>
	    <c:if test="${isOk=='false'}">
	    	<div class="alert-warning">
	    	<h3><c:out value="Svp Retourner vers la page d'inscription"/></h3>
	    	</div>
	    </c:if>
      <h1>Identifiez vous</h1>
      <center>
      <form action="identifier" method="post" class="form-login form-line" >
        <h5>Email</h5>
        <input
          type="email"
          name="email"
          class="form-control"
          placeholder="Votre email "
        />
        <h5>Password</h5>
        <input
          type="password"
          name="password"
          class="form-control"
          placeholder="Votre password "
        />
        <div class="sumbit-login">
         <button type="submit" class="btn btn-primary">Se connecter</button>
        </div>
      </form>
      </center>
    </div>
  </body>
</html>
