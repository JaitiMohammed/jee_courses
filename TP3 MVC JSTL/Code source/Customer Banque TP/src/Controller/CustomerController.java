package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.Customer;
import Dao.CustomerDao;
import Dao.CustomerDaoImpl;

/**
 * Servlet implementation class CustomerController
 */
@WebServlet("/Customer")
public class CustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CustomerDao  customerDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerController() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	customerDao = new CustomerDaoImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		Customer customer = customerDao.findCustomer(id);
		
		
			String address;
		if(customer == null) {
			address= "unkownCustomer.jsp";
			request.getRequestDispatcher(address).forward(request, response);
		}
		if(customer.getSolde() < 0) {
			address= "nagativeBalance.jsp";
			request.setAttribute("badCustomer", customer);
			request.getRequestDispatcher(address).forward(request, response);

		}
		else if(customer.getSolde() < 1000) {
			address= "normalBalance.jsp";
			request.setAttribute("regularCustomer", customer);
			request.getRequestDispatcher(address).forward(request, response);


		}else {
			address= "highBalance.jsp";
			request.setAttribute("elitCustomer", customer);
			request.getRequestDispatcher(address).forward(request, response);


		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
