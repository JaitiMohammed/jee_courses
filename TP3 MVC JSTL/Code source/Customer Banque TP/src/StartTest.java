import java.util.ArrayList;
import java.util.List;

import Dao.Customer;
import Dao.CustomerDao;
import Dao.CustomerDaoImpl;

public class StartTest {

	public static void main(String[] args) {
		
		
		CustomerDao metier = new CustomerDaoImpl();
		
		//Customer c = new Customer("Ahmed",2340.9);
		
		//metier.addCustomer(c.getNom(),c.getSolde());
		
		//metier.UpdateSolde(5699, "Ahmed");
		
		List<Customer> listeCustomers = new ArrayList<Customer>();
		
		listeCustomers=metier.getAllCustomer();
		
		for (Customer customer : listeCustomers) {
			System.out.println(customer.getId());
			System.out.println(customer.getNom());
			System.out.println(customer.getSolde());
		}
		
	}

}
