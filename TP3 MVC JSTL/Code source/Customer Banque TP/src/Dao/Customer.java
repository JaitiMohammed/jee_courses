package Dao;


// Model
public class Customer {
	private int id;
	private String nom;
	private double solde;
	
	public Customer() {
		
	}

	public Customer(String nom, double solde) {
		super();
		this.nom = nom;
		this.solde = solde;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getSolde() {
		return solde;
	}
	
	public double getSoldeNoSign() {
		return (Math.abs(solde));
	}
	public void setSolde(double solde) {
		this.solde = solde;
	}
	
	
	

}
