package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImpl implements CustomerDao{

	@Override
	public double getbalance() {
		
		Connection conn = ConnectionToDB.getConnection();
		Customer c = new Customer();
		double solde = 0;
		String sql = "SELECT SOLDE FROM CUSTOMER WHERE ID = ?" ;
		try {
			PreparedStatement pr = conn.prepareStatement(sql);
			pr.setInt(1, c.getId());
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				solde = rs.getDouble("SOLDE");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return solde;
	}

	@Override
	public void addCustomer(String nom, double solde) {
		Connection conn = ConnectionToDB.getConnection();
		String sql = "INSERT INTO CUSTOMER VALUES (NULL,?,?)";
		
		PreparedStatement pr;
		try {
			pr = conn.prepareStatement(sql);
			pr.setString(1, nom);
			pr.setDouble(2, solde);
			pr.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void UpdateSolde(double solde,String nom) {
		Customer c = new Customer();
		Connection conn = ConnectionToDB.getConnection();
		String sql = "UPDATE CUSTOMER SET SOLDE = ? WHERE NOM = ?";
		
		
		PreparedStatement pr;
		try {
			pr = conn.prepareStatement(sql);
			pr.setDouble(1, solde);
			pr.setString(2,nom);
			pr.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println("Query � �t� bien �x�cuter !");
	}

	@Override
	public int getId(String nom) {
		int id =0;
		Connection conn = ConnectionToDB.getConnection();
		String sql = "SELECT ID FROM CUSTOMER  WHERE NOM = ?";
		try {
			PreparedStatement pr = conn.prepareStatement(sql);
			pr.setString(1, nom);
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				id =rs.getInt("ID");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("Query � �t� bien �x�cuter !");
		return id;
	}

	@Override
	public Customer findCustomer(int id) {
		Customer c = new Customer();
		Connection conn = ConnectionToDB.getConnection();
		String sql = "SELECT ID,NOM ,SOLDE FROM CUSTOMER  WHERE ID = ?";
		PreparedStatement pr;
		try {
			pr = conn.prepareStatement(sql);
			pr.setInt(1, id);
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				c.setId(rs.getInt("ID"));
				c.setNom(rs.getString("NOM"));
				c.setSolde(rs.getDouble("SOLDE"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//System.out.println("Query � �t� bien �x�cuter !");
		return c;
	}

	@Override
	public List<Customer> getAllCustomer() {
		List<Customer> customers = new ArrayList<Customer>();
		Customer c ;
		Connection conn = ConnectionToDB.getConnection();
		String sql = "SELECT * FROM CUSTOMER";
		PreparedStatement pr;
		try {
			pr = conn.prepareStatement(sql);
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				c= new Customer(); // creer objet
				c.setId(rs.getInt("ID"));
				c.setNom(rs.getString("NOM"));
				c.setSolde(rs.getDouble("SOLDE"));
				customers.add(c); // ajout dans le meme objet
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Query � �t� bien �x�cuter !");
		return customers;
	}

	

	

}
