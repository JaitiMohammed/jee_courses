package Dao;

import java.util.List;

public interface CustomerDao {
	
	
	public double getbalance();
	public int getId(String nom);
	void addCustomer(String nom, double solde);
	void UpdateSolde(double solde, String nom);
	public Customer findCustomer(int id);
	public List<Customer> getAllCustomer();
	

}
