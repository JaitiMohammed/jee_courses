<%@page import="Dao.Customer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% Customer c = (Customer) request.getAttribute("regularCustomer"); %>
		You have a normal balance
	<% if (c != null) { %>
	<h1>The result</h1>
	<table border="2">

		<tr>
			<th>ID</th>
			<th>NOM</th>
			<th>SOLDE</th>
		</tr>
		<tr>
			<th><%= c.getId() %></th>
			<th><%= c.getNom() %></th>
			<th><%= c.getSolde() %></th>
		</tr>
	</table>
	<%} %>
</body>
</html>