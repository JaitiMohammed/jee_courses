package Model;

public class Livre {
	private int id;
	private String nom ;
	private String auteur;
	private double prix;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public Livre(int id , String nom, String auteur, double prix) {
		super();
		this.id=id;
		this.nom = nom;
		this.auteur = auteur;
		this.prix = prix;
	}
	public Livre() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
