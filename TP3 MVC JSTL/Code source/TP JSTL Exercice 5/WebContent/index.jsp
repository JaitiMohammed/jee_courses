<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<sql:setDataSource dataSource="jdbcMagasin" />
	<sql:query var="livres" sql="select * from livre where 1 " />

	<table border="1">
		
			<c:forEach var="columnName" items="${livres.columnNames}">
				<th><c:out value="${columnName}" /></th>
			</c:forEach>
			<c:forEach var="row" items="${livres.rows}">
			<tr>
				<c:forEach var="column" items="${row}">
					<td><c:out value="${column.value}"/></td>
				</c:forEach>
			</tr>
		 </c:forEach>
	</table>

</body>
</html>