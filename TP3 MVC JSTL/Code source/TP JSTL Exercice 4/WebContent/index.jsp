<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



	<form action="index.jsp" method="get">
		<c:set value="en" var="en"></c:set>
		<c:set value="fr" var="fr"></c:set>
		
		Choose your language
		
		
		<select name='choix' >
			<option value="${en}">${en}</option>
			<option value="${fr}">${fr}</option>			
		</select>
		<br/>
		
		<c:set var="choice" value="${param.choix}"></c:set>	
		
		<c:if test="${choice=='en'}">
			<c:set var="text_langue" value="text_en"></c:set>
		</c:if>
		<c:if test="${choice=='fr'}">
			<c:set var="text_langue" value="text_fr"></c:set>
		</c:if>
		
		
		
		
		<c:set var="langugae" value="langages.${text_langue}"></c:set>
		
		
		<fmt:setBundle basename="${langugae}" var="choixL" />
		<label> <fmt:message key="login.label.username"
				bundle="${choixL}"></fmt:message>
		</label><br /> <input type="text" name="username"> <br /> <label> <fmt:message
				key="login.label.mpd" bundle="${choixL}"></fmt:message>
		</label><br /> <input type="password" name="psw" ><br />
		<button type="submit">
			<fmt:message key="login.button.submit" bundle="${choixL}"></fmt:message>
		</button>
	</form>
	
	<c:redirect url="vue.jsp">
		<c:param name="username" value="${param.username}"></c:param>
		<c:param name="languge" value="${param.choix}"/>
	</c:redirect>

</body>
</html>