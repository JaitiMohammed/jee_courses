package Modele;

import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IMetierImpl implements IMetierDao {

	@Override
	public List<Etudiant> getAllEtudiant(String promoname) {
		Connection conn = ConnexionDB.getConnection();
		List<Etudiant> Etudiants = new ArrayList<Etudiant>();
		String sql = "SELECT * FROM ETUDIANT WHERE promoname = ?";
		try {
			PreparedStatement pr = conn.prepareStatement(sql);
			pr.setString(1,promoname);
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				Etudiant e = new Etudiant();
				e.setNom(rs.getString("NOM"));
				e.setPrenom(rs.getString("PRENOM"));
				e.setPromotion(rs.getString("PROMONAME"));
				e.setAge(rs.getInt("AGE"));
				Etudiants.add(e);

			}
			pr.close();
		} catch (SQLException er) {
			er.printStackTrace();
		}
		return Etudiants;
	}


}
