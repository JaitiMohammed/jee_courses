<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1" http-equiv="Content-Type" content="text/html">
<title>Insert title here</title>
</head>
<body>
	<form action="index.jsp" method="get">
	<select >
		<option value='<c:set var="us" value="en_us"></c:set>'>US</option>
		<option>Fr</option>
	</select>
	</form>
	Now
	<c:set var="today" value="<%=new java.util.Date()%>"></c:set>
	<p>
		Time : <strong><fmt:formatDate type="time" value="${today}" /></strong>
	</p>
	<p>
		Date : <strong><fmt:formatDate type="date" value="${today}" /></strong>
	</p>
	<p>
		Date & Time : <strong><fmt:formatDate type="both"
				value="${today}" /></strong>
	</p>
	<p>
		Date & Time Short : <strong><fmt:formatDate type="both"
				dateStyle="short" timeStyle="short" value="${today}" /></strong>
	</p>
	
	<p>Currency 
		<fmt:setLocale value="en_us"/>
		<fmt:formatNumber value="25.8" type="currency"></fmt:formatNumber>

	</p>
	


</body>
</html>