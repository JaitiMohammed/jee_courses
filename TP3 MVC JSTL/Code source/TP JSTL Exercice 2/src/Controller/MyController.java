package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Modele.Etudiant;
import Modele.IMetierDao;
import Modele.IMetierImpl;
import Modele.promo;

/**
 * Servlet implementation class MyController
 */
@WebServlet("/controller")
public class MyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IMetierDao metier ;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyController() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	metier = new IMetierImpl();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String promo = request.getParameter("promo");
		
		Etudiant e1 = new Etudiant("Jaiti" ,"Mohammed",21,"ISIL2019");
		Etudiant e2= new Etudiant("Ajbabdi" ,"Wissal",21,"ISIL2019");
		Etudiant e3 = new Etudiant("Zennou" ,"Khalid",21,"ISIL2019");
		
		promo p = new promo();
		p.addEtudiant(e1);
		p.addEtudiant(e2);
		p.addEtudiant(e3);
		

		List<Etudiant> myList = metier.getAllEtudiant(p,promo);

		request.setAttribute("result", myList);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
