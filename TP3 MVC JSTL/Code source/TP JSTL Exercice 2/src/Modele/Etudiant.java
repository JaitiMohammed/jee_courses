package Modele;

import java.util.ArrayList;
import java.util.List;

public class Etudiant {
	
	private String nom ;
	private String prenom;
	private int age ;
	private String promotion;
	
	private List<Etudiant> Etudiants = new ArrayList<Etudiant>();
	
	
	public Etudiant(String nom, String prenom, int age, String promotion) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.promotion = promotion;
	}

	public Etudiant() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	
	
	
}
