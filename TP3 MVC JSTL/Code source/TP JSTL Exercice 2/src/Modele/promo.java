package Modele;

import java.util.ArrayList;
import java.util.List;

public class promo {
	private String promotion;

	private List<Etudiant> Etudiants = new ArrayList<Etudiant>();

	
	public promo() {
		
	}
	public List<Etudiant> getEtudiants() {
		return Etudiants;
	}
	
	public void setEtudiants(List<Etudiant> etudiants) {
		Etudiants = etudiants;
	}
	
	public void addEtudiant(Etudiant e) {
		Etudiants.add(e);
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	
	

}
