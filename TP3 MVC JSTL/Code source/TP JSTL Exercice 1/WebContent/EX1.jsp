<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Exercice 1</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>Nombre</th>
		</tr>
		<c:forEach var="i" begin="3" end="15" step="2">
			<tr>
				<td bgcolor="green"><c:out value="${i}" /></td>
			</tr>
			<tr>
				<td bgcolor="red"><c:out value="${i+1}" /></td>
			</tr>
		</c:forEach>


	</table>

</body>
</html>