package exerccie4.formulaire;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class formulaire
 */
@WebServlet("/formulaire")
public class formulaire extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Date actuelle ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public formulaire() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	actuelle = new Date();
    	
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String civilite = request.getParameter("civilite");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		int age = Integer.parseInt(request.getParameter("age"));
		
		response.setContentType("text/html");
		DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		String date = dateFormat.format(actuelle);
		
		try (PrintWriter  pr = response.getWriter()){
			pr.println("<DOCTYPE html>");
			pr.println("<html>");
				pr.println("<head>");
					pr.println("<title>formulaire</title>");
				pr.println("</head>");
				pr.println("<body>");
					pr.println("Binevenu");
					pr.println("<h2>"+date + "</h2>");
					pr.println("<h3> Civilit� " + civilite + "</h3>");
					pr.println("<h3> Nom " + nom + "</h3>");
					pr.println("<h3> Prenom " + prenom + "</h3>");
					pr.println("<h3> age " + age + "</h3>");
					
				pr.println("</body>");
			pr.println("</html>");
			pr.flush();
			pr.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
