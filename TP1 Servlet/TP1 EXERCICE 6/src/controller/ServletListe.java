package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletListe
 */
@WebServlet("/servletListe")
public class ServletListe extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, String> map ;
     
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListe() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	map = new HashMap<String, String>();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter pr = response.getWriter();
		response.setContentType("text/html");
		pr.println("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset=\"ISO-8859-1\">\r\n" + 
				"<title>Servlet List</title>\r\n" + 
				"</head>\r\n" + 
				"<body bgcolor=\"yellow\">\r\n" + 
				"<h3>Add</h3>\r\n" + 
				"<P>\r\n" + 
				"<form action=\"servletListe\" method=\"POST\">\n" + 
				"Titre:\r\n" + 
				"<input type=text size=20 name=\"titre\">\r\n" + 
				"<br>\r\n" + 
				"Description:\r\n" + 
				"<input type=text size=20 name=\"description\">\r\n" + 
				"<br>\r\n" + 
				"<input type=\"submit\"  value=\"Add\">\r\n" + 
				"</form>\r\n" + 
				"	<hr>\r\n" + 
				"	\r\n" + 
				"	\r\n" + 
				"	</table>\r\n" + 
				"</body>\r\n" + 
				"</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
		
		String titre = request.getParameter("titre");
		String desc = request.getParameter("description");
		
		
		PrintWriter pr = response.getWriter();
		map.put(titre, desc);
		
		
		
		
		//PrintWriter pr = response.getWriter();
		
		Set<Entry<String, String>> set = map.entrySet();
		
		Iterator<Entry<String, String>> it = set.iterator();
		pr.println("<table border='1'>");
		pr.println("<tr><th>titre</th>");
		pr.println("<th>description</th></tr>");
		pr.println("<tr>");
		while(it.hasNext()) {
			Entry<String, String> key = it.next();
			pr.println("<td>"+key.getKey() +"</td>");
			pr.println("<td>"+key.getValue() +"</td>");
		}
		pr.println("</tr>");
		
		pr.println("</table>");
		
		
	}

}
