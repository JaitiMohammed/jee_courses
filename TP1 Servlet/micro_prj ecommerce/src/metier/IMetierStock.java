package metier;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public interface IMetierStock {
	
	public void ajouterDisque(Disque d);
	public List<Disque> afficheDisque() throws SQLException;
	public void viderPanier();
	public void supprimerDisque(String nomDisque);
	public void rechercherDisque(String nomDique);
	public void vente(PrintWriter out);
}
