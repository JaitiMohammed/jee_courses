package metier;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DataBase.DataBaseContext;

public class Stock {

	public static String[][] getLeStock() {
		return leStock;
	}

	public static void setLeStock(String[][] leStock) {
		Stock.leStock = leStock;
	}

	static String[][] leStock = {
			{ "Disque CD - AMOR TICINES", "15", "disque897TR566" },
			{ "Disque CD - Los Mayas", "19", "disque78UUNYT67" },
			{ "Disque CD - Dick Anglas", "25", "disque87YHG564" },
			{ "Disque CD - Frederic Angonas", "35", "disque98HUYU56" },
			{ "Disque DVD - Record new ", "45", "disqueE8HHYU56" }
			};

	public void vente(PrintWriter out) {
		out.println("<center><table border=1>");
		out.println("<tr bgColor='orange'><th>Nom Disque</th> " + "<th>Comm</th>" + "</tr>");
		for (int i = 0; i < leStock.length; i++) {
			;
			out.println("<tr> <td>" + leStock[i][0] + " " + leStock[i][1] + " Euros </td>");
			out.println(" <td><A HREF=\"commande?element=disque&code=");
			out.println(leStock[i][2] + "&ordre=ajouter&prix=" + leStock[i][1] + "\">");
			out.println("<IMG SRC=\"/WebContent/imgs/panier.jpg\" BORDER=0></A><br> </td> </tr>");
		}
		out.println("</table></center>");
	}

}
