package metier;

public class Disque {
	private String nom ;
	
	public Disque(String nom, String auteur, double prix, String desc) {
		super();
		this.nom = nom;
		this.auteur = auteur;
		this.prix = prix;
		this.desc = desc;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	private String auteur ;
	private double prix ;
	private String desc ;
	public Disque() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Disque(String auteur, double prix, String desc) {
		super();
		this.auteur = auteur;
		this.prix = prix;
		this.desc = desc;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

}
