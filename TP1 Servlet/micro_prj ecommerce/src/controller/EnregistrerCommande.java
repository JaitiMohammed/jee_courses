package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DataBase.DataBaseContext;

import java.sql.*;

import metier.Identification;

/**
 * Servlet implementation class EnregistrerCommande
 */
@WebServlet("/servlet/enregistre")
public class EnregistrerCommande extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection conn ;
	private Statement stmt = null;
	private PreparedStatement pstmt = null;
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EnregistrerCommande() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nom = null;
		//HttpSession session = request.getSession();
		int nbreProduit = 0;
		Cookie[] cookies = request.getCookies();
		boolean connu = false;
		nom = Identification.chercheNom(cookies);
		OuvreBase();
		//AjouteNomBase(nom);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<head>");
		out.println("<title> votre commande </title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("<h3>" + "Bonjour " + nom + " voici ta nouvelle commande" + "</h3>");
		HttpSession session = request.getSession();
		//Enumeration names = session.getAttributeNames();
		
		/*session.getAttribute("element");
		session.getAttribute("code");
		session.getAttribute("prix");*/
		
		/*
		while (names.hasMoreElements()) {
			nbreProduit++;
			String name = (String) names.nextElement();
			String value = session.getAttribute(name).toString();
			out.println(name + " = " + value + "<br>");
		}*/
		AjouteCommandeBase(nom, session);
		out.println("<h3>" + "et voici " + nom + " ta commande complete" + "</h3>");
		
		MontreCommandeBase(session, out);
		
		out.println("<A HREF=mesCommandes.VidePanier> Vous pouvez commandez un autre disque </A><br> ");
		out.println("</body>");
		out.println("</html>");
	}

	protected void OuvreBase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/microecommerce","root","");
			System.err.println("Connextion �tablie ");
			stmt = conn.createStatement();
			}
			catch (Exception E) {
			log(" -------- probeme " + E.getClass().getName() );
			E.printStackTrace();
			}
	}

	protected void fermeBase() {
		try {
			stmt.close();
			conn.close();
		} catch (Exception E) {
			log(" -------- probl�me " + E.getClass().getName());
			E.printStackTrace();
		}
	}

	/*protected void AjouteNomBase(String nom) {
		try {
			ResultSet rset = null;
			pstmt = conn.prepareStatement("select numero from personnel where nom=?");
			pstmt.setString(1, nom);
			rset = pstmt.executeQuery();
			if (!rset.next())
				pstmt = conn.prepareStatement("INSERT INTO personnel VALUES (nom)");
				pstmt.setString(1, nom);
				pstmt.executeUpdate();
		} catch (Exception E) {
			log(" - probeme " + E.getClass().getName());
			E.printStackTrace();
		}
	}*/
	protected void AjouteCommandeBase(String nom, HttpSession session ) {
		String commande = (String)session.getAttribute("code") ;
		String sql = "INSERT INTO commande VALUES (?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, 1);
			pstmt.setString(2,commande);
			pstmt.setString(3,nom);
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} 
	protected void MontreCommandeBase(HttpSession session, PrintWriter out) {
		out.println("<center><table border=1>");
		out.println("<tr bgColor='orange'><th>Nom Disque</th></tr>");
		out.println("<tr><td> " + session.getAttribute("element")  + session.getAttribute("prix") + " Euros " +  " - " + session.getAttribute("code") + " </td></tr>");
		out.println("</table></center><br/>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
