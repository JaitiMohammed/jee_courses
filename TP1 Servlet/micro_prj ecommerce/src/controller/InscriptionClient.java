package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class InscriptionClient
 */
@WebServlet("/servlet/sinscrire")
public class InscriptionClient extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String nomRecu=null, motPasseRecu=null;
	private String nomCookie=null, motPasseCookie=null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscriptionClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
		nomRecu = request.getParameter("nom");
		motPasseRecu = request.getParameter("motdepasse");
		
		
		/*
		Cookie[] allcookies = request.getCookies();
		for(int i=0 ; i< allcookies.length ; i++) {
			if(allcookies[i].getName().equals("nomC")) {
				nomCookie = allcookies[i].getValue();
			}
		}
		*/
	
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if (nomCookie==null && nomRecu==null){
			out.println("<html>");
			out.println("<body>");
			out.println("<head>");
			out.println("<title> inscription d'un client </title>");
			out.println("</head>");
			out.println("<body bgcolor='white' >");
			out.println("Nom recu " + nomRecu +" |pass recu : "+ motPasseRecu +" | nom Cookie : "+ nomCookie +" | mot de passe Cookie :  "+ motPasseCookie );
			out.println("<h3>" + "Bonjour, vous devez vous inscrire " + "</h3>");
			out.println("<h3>" 
			+ "Attention mettre nom et le mot de passe avec plus de 3 caracteres" +
			"</h3>");
			out.print(" <form action='sinscrire' method='GET' > ");
			out.println("nom");
			out.println("<input type='text' size='20' name='nom' >");
			out.println("<br>");
			out.println("mot de passe");
			out.println("<input type='password' size='20' name='motdepasse'> <br>");
			out.println("<input type='submit' value='inscription'>");
			out.println("</form>");
			out.println("</body>");
			out.println("</html>");
	
			}else if (nomCookie==null && nomRecu!=null){
				
				Cookie nomC = new Cookie("nomC", nomRecu);
				nomC.setMaxAge(60*60);
				Cookie passC = new Cookie("motPC", motPasseRecu);
				motPasseCookie = passC.getValue();
				nomCookie = nomC.getValue();
				passC.setMaxAge(60*60);

				response.addCookie(nomC);
				response.addCookie(passC);
				out.println("Nom recu " + nomRecu +" |pass recu : "+ motPasseRecu +" | nom Cookie : "+ nomCookie +" | mot de passe Cookie :  "+ motPasseCookie );
				out.println("<h3>" + "Bonjour, vous devez authentifi�" + "</h3>");
				out.println("<h3>" 
				+ "Attention mettre nom et le mot de passe avec plus de 3 caracteres" +
				"</h3>");
				out.print(" <form action='sinscrire' method='GET' > ");
				out.println("nom");
				out.println("<input type='text' size='20' name='nom' >");
				out.println("<br>");
				out.println("mot de passe");
				out.println("<input type='password' size='20' name='motdepasse'> <br>");
				out.println("<input type='submit' value='authentif�'>");
				out.println("</form>");
				out.println("</body>");
				out.println("</html>");
				
			}else if (identique(nomRecu,nomCookie) && identique(motPasseRecu,motPasseCookie)) {
				HttpSession session = request.getSession(true);
				session.setAttribute("nomSession", nomCookie);
				session.setAttribute("passSession", motPasseCookie);
				out.println("<h1> Bienvenu " + nomCookie +"</h1>");
				//session.getServletContext().getRequestDispatcher("achat.html");
			}else {
				
				out.println("<h3>" 
				+ " votre authentification n'est pas accessible v�rifiez vos donn�es " +
				"</h3>");
				out.println("<h3>" + "r�ssayez encore une fois ou s'inscrire " + "</h3>");
				out.println("<h3>" 
				+ "Attention mettre nom et le mot de passe avec plus de 3 caracteres" +
				"</h3>");
				out.print(" <form action='sinscrire' method='GET' > ");
				out.println("nom");
				out.println("<input type='text' size='20' name='nom' >");
				out.println("<br>");
				out.println("mot de passe");
				out.println("<input type='password' size='20' name='motdepasse'> <br>");
				out.println("<input type='submit' value='inscription'>");
				out.println("</form>");
				out.println("</body>");
				out.println("</html>");
				
			}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	boolean identique (String recu, String cookie) {
		return ((recu != null) && (recu.length() >3) && (cookie != null) && (recu.equals(cookie) ));
	}

}
