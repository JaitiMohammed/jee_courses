package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DataBase.DataBaseContext;
import metier.IMetierStock;
import metier.Identification;
import metier.Stock;

/**
 * Servlet implementation class AfficherLesDisques
 */
@WebServlet("/servlet/achat")
public class AfficherLesDisques extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private IMetierStock metierStock;
	private Connection conn ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    
    @Override
    	public void init() throws ServletException {
    		// TODO Auto-generated method stub
    		super.init();
    		//metierStock = new Stock();
    		conn= DataBaseContext.init(this.getServletContext());
    	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Stock uneVente = new Stock();
		String nom = null;
		Cookie[] cookies = request.getCookies();
		HttpSession session = request.getSession();
		session.setAttribute("uneVente", uneVente);
		nom = Identification.chercheNom(cookies);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<head>");
		out.println("<title> Commande de disques </title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("<center><h3>" + "Bonjour " + nom + " vous pouvez commander un disque" + "</h3></center>");
		uneVente.vente(out);
		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
