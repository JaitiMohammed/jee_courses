package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.Identification;
import metier.Stock;

/**
 * Servlet implementation class CommanderUnDisque
 */
@WebServlet("/servlet/commande")
public class CommanderUnDisque extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommanderUnDisque() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = null;
		Stock uneVente = new Stock();
		int nbreProduit = uneVente.getLeStock().length;
		HttpSession session = request.getSession();
		session.getAttribute("uneVente");
		Cookie[] cookies = request.getCookies();
		nom = Identification.chercheNom(cookies);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<head>");
		out.println("<title> votre commande </title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("<center><h3>" + "Bonjour "+ nom + " voici votre commande" + "</h3>");
		
		uneVente.vente(out);
		out.println("<h1>");
		out.println("<h3>Votre panier contient " + nbreProduit + " disque(s) </h3>");
		
		String element = request.getParameter("element");
		String code = request.getParameter("code");
		String ordre = request.getParameter("ordre");
		int prix = Integer.parseInt(request.getParameter("prix"));
		
		
		session.setAttribute("element", element);
		session.setAttribute("code", code);
		session.setAttribute("ordre", ordre);
		session.setAttribute("prix", prix);
		
		out.println("<br/><A HREF=achat> Vous pouvez commandez un autre disque </A><br> ");
		out.println("<A HREF=enregistre> Vous pouvez enregistrer votre commande </A><br> </center>");
		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
