package DataBase;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;

public class DataBaseContext {
	protected static String dbURL;
	protected static String dbLogin;
	protected static String dbPassword;
	protected static Connection con = null;
	
	public static Connection init(ServletContext context) {
		
		try {
			Class.forName(context.getInitParameter("JDBC_DRIVER"));
			dbURL = context.getInitParameter("JDBC_URL");
			dbLogin = context.getInitParameter("JDBC_LOGIN");
			dbPassword = context.getInitParameter("JDBC_PASSWORD");
			try {
				con = DriverManager.getConnection(dbURL,dbLogin,dbPassword);
				System.out.println("Connextion bien Etabl�");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}
}
