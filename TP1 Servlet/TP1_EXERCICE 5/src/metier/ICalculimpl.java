package metier;

public class ICalculimpl implements ICalculMetier{
	 
	@Override
	public int calculer(int operande1, int operande2, String operation) {
		int result = 0;
		if(operation.equals("Addition")) {
			result = operande1 + operande2;
		}
		if(operation.equals("Soustraction")) {
			result=operande1 - operande2;
		}
		if(operation.equals("Multiplication")) {
			result=operande1 * operande2;
		}
		if(operation.equals("Division")) {
			result=operande1 / operande2;
		}
		return result;
	}

}
