package controller.calcul;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.calcul.*;

import metier.ICalculimpl;

import metier.ICalculMetier;

/**
 * Servlet implementation class calcul
 */
@WebServlet(urlPatterns = {"/calcul"},loadOnStartup = 1)
public class calcul extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ICalculMetier calcul ;
       
	private List<Integer> resultats;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public calcul() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	calcul = new ICalculimpl();
    	resultats = new ArrayList<Integer>();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter pr = response.getWriter();
		
		/*int number1 = Integer.parseInt(request.getParameter("number1"));
		int number2 = Integer.parseInt(request.getParameter("number2"));
		String operation = request.getParameter("operation");
		ResultModel model = new ResultModel();
		
		model.setOperande1(number1);
		model.setOperande2(number2);
		model.setOperation(operation);
		
	
		
		int result = calcul.calculer(number1, number2, operation);
		model.setResult(result);
		
		*/
		pr.print("<center><h1>Calculatrice</h1></center>\r\n" + 
				"	\r\n" + 
				"	<form action=\"calcul\" method=\"post\">\r\n" + 
				"		<div id=\"calculer\">\r\n" + 
				"		<label>Entrer Oprande 1 : </label>\r\n" + 
				"		<input  type='text' name='number1' placholder='type your number' ><br/>\r\n" + 
				"		<label >Entrer l'Op�ration  : </label>\r\n" + 
				"			<select name=\"operation\">\r\n" + 
				"				<option >Addition</option>\r\n" + 
				"				<option >Division</option>\r\n" + 
				"				<option >Multiplication</option>\r\n" + 
				"				<option >Soustraction</option>\r\n" + 
				"			</select><br/>\r\n" + 
				"		<label>Entrer Oprande 2 : </label>\r\n" + 
				"		<input  type='text' name='number2' placholder ='type your number' >\r\n" + 
				"		<br/>\r\n" + 
				"		<button type=\"submit\">Calcul�</button>\r\n" + 
				"		</div>\r\n" + 
				"		\r\n" + 
				"		\r\n" + 
				"	</form>\r\n" + 
				"	");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
		
		PrintWriter pr = response.getWriter();
		
		int number1 = Integer.parseInt(request.getParameter("number1"));
		int number2 = Integer.parseInt(request.getParameter("number2"));
		String operation = request.getParameter("operation");
		ResultModel model = new ResultModel();
		
		/*
		model.setOperande1(number1);
		model.setOperande2(number2);
		model.setOperation(operation);
		*/
	
		
		int result = calcul.calculer(number1, number2, operation);
		resultats.add(result);
		//model.setResult(result);
		
		pr.print("<center><h2>le r�sultat est "+result+":</h2></center>");
		pr.print("<center><h2>liste des r�sultats pr�cedents : "+resultats+":</h2></center>");
		
		
		
	}

}
