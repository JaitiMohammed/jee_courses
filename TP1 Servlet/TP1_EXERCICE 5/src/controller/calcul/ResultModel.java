package controller.calcul;

public class ResultModel {
	private String operation ;
	private int Operande1;
	private int Operande2;
	private int result;
	public ResultModel(String operation, int operande1, int operande2, int result) {
		super();
		this.operation = operation;
		Operande1 = operande1;
		Operande2 = operande2;
		this.result = result;
	}
	public ResultModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getOperande1() {
		return Operande1;
	}
	public void setOperande1(int operande1) {
		Operande1 = operande1;
	}
	public int getOperande2() {
		return Operande2;
	}
	public void setOperande2(int operande2) {
		Operande2 = operande2;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	
	
	
	
}
